<?php


class QRClientsOrganisation extends QRClientsCommon {
        function __construct() {
                parent::__construct( 'QRClientsOrganisation' );
//                wfLoadExtensionMessages('QRClientsOrganisation');
        }
 
        function rawExecute( $par ) {
                global $wgRequest;
 
                $this->setHeaders();
                $meth = $wgRequest->getMethod();
                switch ($meth) {
                    case "GET":
                        $this->methodGET();
                        break;
                    case "POST":
                        $this->methodPOST();
                        break;
                    default:
                        $this->renderNothing();
                }
                
        }
        
        function methodPOST() {
            global $wgRequest, $wgOut;
            
            $action = $wgRequest->getText('action');
            switch ($action) {
                case "create":
                    $this->actionCreate();
                    break;
                case "update":
                    $this->actionUpdate();
                    break;
                case "delete":
                    $this->actionDelete();
                    break;
                default:
                    $this->renderNothing("параметр action имеет не верное значение");
            }
        }
        
        function actionUpdate() {
            global $wgRequest;
            $id = $wgRequest->getText('id');
            $name = $wgRequest->getText('organisation_name');
            if ( empty($id) || empty($name) ) {
                $this->renderNothing( "не указан один из параметров id, organisation_name" );
                return;
            }
            
            $dbr = wfGetDB(DB_MASTER);
            $res = $dbr->select('qrclient_organisations',
                                array('organisation_name'),
                                array('organisation_id' => $id));
            $row = $res->fetchRow();
            if ( empty($row) ) {
                $this->renderNothing( "нет организации id=$id" );
                return;                
            }
            
            $dbr->update('qrclient_organisations',
                         array('organisation_name' => $name),
                         array('organisation_id' => $id));
            $dbr->commit();
            
            header('Location: /Special:QRClientsOrganisation?' . http_build_query(array('id' => $id)));
            exit;
            
        }
        
        function actionDelete() {
            global $wgRequest;
            $id = $wgRequest->getText('id');
            if ( empty($id) ) {
                $this->renderNothing( "не указан параметр id" );
                return;
            }
            
            $dbr = wfGetDB(DB_MASTER);
            $res = $dbr->select('qrclient_organisations',
                                array('organisation_name'),
                                array('organisation_id' => $id));
            $row = $res->fetchRow();
            if ( empty($row) ) {
                $this->renderNothing( "нет организации id=$id" );
                return;                
            }
            $dbr->delete('qrclient_organisations',
                         array('organisation_id' => $id));
            $dbr->commit();
            
            header('Location: /Special:QRClientsOrganisations');
            exit;
            
        }
        
        function actionCreate() {
            global $wgOut, $wgRequest;
            
            $name = $wgRequest->getText('organisation_name');
            if (empty($name)) {
                $this->renderNothing('не указано имя организации');
                return;
            }
            
            $dbr = wfGetDB(DB_MASTER);
            $dbr->insert('qrclient_organisations', array('organisation_name' => $name));
            $id = $dbr->insertId();
            $dbr->commit();
            header('Location: /Special:QRClientsOrganisation?' . http_build_query(array('id' => $id)));
            exit;
        }
        
        function methodGET() {
            global $wgRequest, $wgOut;
            
            $action = $wgRequest->getText('action');
            switch ($action) {
                case "new":
                    $this->actionNew();
                    break;
                case "edit":
                    $this->actionEdit();
                    break;
                case "show":
                case "":
                    $this->actionShow();
                    break;
                default:
                    $this->renderNothing("параметр action имеет не верное значение");
            }
        }
        
        function actionEdit() {
            global $wgRequest;
            $id = $wgRequest->getText('id');
            if ( empty($id) ) {
                $this->renderNothing( "не указан параметр id" );
                return;
            }
            
            $dbr = wfGetDB(DB_SLAVE);
            $res = $dbr->select('qrclient_organisations',
                                array('organisation_name'),
                                array('organisation_id' => $id));
            $row = $res->fetchRow();
            if ( empty($row) ) {
                $this->renderNothing( "нет организации id=$id" );
                return;                
            }
            
            $orgname = $row['organisation_name'];
            $this->renderEditForm( $id, $orgname, "update", "Обновить" );
        }
        
        function actionNew() {
            $this->renderEditForm( "", "", "create", "Создать" );
        }
        
        function renderEditForm( $id, $name, $action, $verb ) {
            global $wgOut;
            
            ob_start();
            ?>
            <form method="POST" action="/Special:QRClientsOrganisation?action=<?php echo $action ?>">
                <?php if ( ! empty($id) ): ?>
                    <input type="hidden" name="id" value="<?php echo $id ?>"></input>
                <?php endif ?>    
                <input type="text" name="organisation_name" placeholder="имя организации" value="<?php echo $name ?>"></input>
                <input type="submit" value="<?php echo $verb ?>"></input>
            </form>
            <?php
            $cont = ob_get_contents();
            ob_end_clean();
            $wgOut->addHTML($cont);
        }
        
        function actionShow() {
            global $wgRequest, $wgOut;
            $id = $wgRequest->getText('id');
            if (empty($id)) {
                $this->renderNothing();
                return;
            }
            
            $dbr = wfGetDB( DB_SLAVE );
            $res = $dbr->select('qrclient_organisations', array('organisation_id', 'organisation_name'),
                                array('organisation_id' => $id));
            $row = $res->fetchRow();
            if (empty($row)) {
                $this->renderNothing("нет организации с id = $id");
                return;
            }
            $name = $row['organisation_name'];
            
            $wgOut->addHTML('<a href="/Special:QRClientsOrganisations">К списку организаций</a>');
            $wgOut->addWikiText( "== $name ==");
            $wgOut->addHTML(  '<a href="/Special:QRClientsOrganisation?'
                            . http_build_query(array('action' => 'edit',
                                                     'id' => $id))
                            . '">Переименовать</a>');
            $wgOut->addWikiText( "=== Свойства ===" );
            $this->renderProperties( $id );
            $wgOut->addHTML( '<a href="/Special:QRClientsOrgProp?'
             . http_build_query(array('action' => 'new',
                                      'organisation_id' => $id))
             . '">Создать новое</a>' );
            $wgOut->addWikiText( "=== Страницы ===" );
            $this->renderPages( $id );
            $wgOut->addHTML( '<a href="/Special:QRClientsOrgPage?'
             . http_build_query(array('action' => 'new',
                                      'organisation_id' => $id))
             . '">Привязать новую</a>' );
        }
        
        function renderProperties( $id ) {
            global $wgOut;
            $dbr = wfGetDB(DB_SLAVE);
            $res = $dbr->select('qrclient_org_propery', array('property_id', 'property_name', 'property_value'),
                                array('property_org_id' => $id));
            
            
            ob_start();
            ?>
            <script type="text/javascript">
                function MAKEPOST(addr) {
                    yes = confirm('Удалить ?');
                    if (yes) {
                        $.post(addr, function() {
                            window.location.reload()
                        });
                    }
                }
            </script> 
            
            <table>
                <?php while($row = $res->fetchRow()): ?>
                <tr>
                    <td>
                        <a href="<?php echo '/Special:QRClientsOrgProp?' 
                          . http_build_query(
                              array('property_id' => $row['property_id'],
                                    'action' => 'edit'))?>">
                        <?php echo $row['property_name'] . ': ' . $row['property_value'] ?>
                        </a>
                    </td>
                    <td>
                        <a href="#" onclick="MAKEPOST('<?php 
                        echo "/Special:QRClientsOrgProp?"
                          . http_build_query(array('action' => 'delete',
                                                   'property_id' => $row['property_id']));?>')">
                       Удалить</a>
                    </td>
                </tr>    
                <?php endwhile ?>
            </table>
            <?php
            $cont = ob_get_contents();
            ob_end_clean();
            $wgOut->addHTML($cont);
        }
        
        function renderPages( $id ) {
            global $wgOut;
            $dbr = wfGetDB(DB_SLAVE);
            $res = $dbr->select('qrclient_org_page', array('orgpage_id', 'orgpage_page_id'),
                                array('orgpage_org_id' => $id));
            
            
            ob_start();
            ?>
            <table>
                <?php while($row = $res->fetchRow()): ?>
                </tr>
                    <td>
                        <a href="<?php echo '/Special:QRClientsOrgPage?' . http_build_query(
                                                                           array('orgpage_id' => $row['orgpage_id']))?>">
                        <?php echo $this->getPageName($row['orgpage_page_id']) ?>
                        </a>
                    </td>
                    <td>
                        <a href="#" onclick="MAKEPOST('<?php echo
                           "/Special:QRClientsOrgPage?"
                         . http_build_query(array('action' => 'delete',
                                                  'orgpage_id' => $row['orgpage_id']));?>')">
                          Отвязать
                        </a>
                    </td>
                </tr>    
                <?php endwhile ?>
            </table>
            <?php
            $cont = ob_get_contents();
            ob_end_clean();
            $wgOut->addHTML($cont);
            
        }
        
        function getPageName( $pid ) {
            $a = Article::newFromID( $pid );
            $t = $a->getTitle();
            return $t->getText();
        }
        
        function renderNothing($text = "Не могу отобразить содержимое") {
            global $wgOut;
            
            $wgOut->addWikiText("== $text ==");
        }
}
