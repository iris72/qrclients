<?php


class QRClientsMain extends QRClientsCommon {
        function __construct() {
                parent::__construct( 'QRClientsMain' );
//                wfLoadExtensionMessages('QRClientsMain');
        }
 
        function rawExecute( $par ) {
            global $wgRequest, $wgOut;
 
            $this->setHeaders();
            
            ob_start();
            ?>
            <p>
                <a href="/Special:QRClientsServices">Список услуг</a>
            </p>
            <p>
                <a href="/Special:QRClientsOrganisations">Список организаций</a>
            </p>
            <?php
            $cont = ob_get_contents();
            ob_end_clean();
            $wgOut->addHTML($cont);

        }
}
