<?php


class QRClientsPagePayments extends QRClientsCommon {
        function __construct() {
                parent::__construct( 'QRClientsPagePayments' );
//                wfLoadExtensionMessages('QRClientsPagePayments');
        }
 
        function rawExecute( $par ) {
            global $wgRequest, $wgOut;
 
            $this->setHeaders();
            
            $pageid = $wgRequest->getText('page_id');
            $serviceid = $wgRequest->getText('service_id');
            if ( empty($pageid) || empty($serviceid)) {
                $this->renderNothing( "Нет параметра page_id или service_id" );
                return;
            }
            
            $t = Title::newFromID($pageid);
            if ( ! $t->exists() ) {
                $this->renderNothing( "Нет страницы с id=$pageid" );
                return;
            }
            $pagename = $t->getText();
            
            $dbr = wfGetDB(DB_SLAVE);
            $res = $dbr->select('qrclient_service',
                                array('service_id', 'service_name'),
                                array('service_id' => $serviceid));
            $row = $res->fetchRow();
            if ( empty($row) ){
                $this->renderNothing( "Нет такой услуги id=$serviceid" );
                return;
            }
            
            
            $this->renderPayedList($serviceid, $row['service_name'], $pageid, $pagename);
        }

        function renderPayedList($serviceid, $servicename, $pageid, $pagename) {
            global $wgOut;
            
            $dbr = wfGetDB(DB_SLAVE);
            
            $res = $dbr->select('qrclient_page_payment',
                               array('pagepayment_id',
                                     'pagepayment_service_id',
                                     'pagepayment_page_id',
                                     'pagepayment_start_date',
                                     'pagepayment_termination_date'),
                               array('pagepayment_service_id' => $serviceid,
                                     'pagepayment_page_id' => $pageid), 
                               'DatabaseBase::select',
                               array('ORDER BY' => 'pagepayment_termination_date DESC'));
            $wgOut->addWikiText("== Страница ==");
            $wgOut->addHTML(  '<a href="/Special:QRClientsPage?'
                            . http_build_query(array('page_id' => $pageid))
                            . '">' . $pagename . '</a>');
            $wgOut->addWikiText("== Услуга ==");
            $wgOut->addHTML(  '<a href="/Special:QRClientsService?'
                            . http_build_query(array('service_id' => $serviceid))
                            . '">' . $servicename . '</a>');                               
                                                           
            $wgOut->addWikiText('== Список оплат ==');
            ob_start();
            ?>
            <script type="text/javascript">
                function MAKEPOST(addr) {
                    yes = confirm('Удалить ?');
                    if (yes) {
                        $.post(addr, function() {
                            window.location.reload()
                        });
                    }
                }
            </script> 

            <table>
                <?php while($row = $res->fetchRow()): ?>
                    <tr>
                        <td>
                            <?php echo $row['pagepayment_start_date'] ?>
                        </td>
                        <td>
                            <?php echo $row['pagepayment_termination_date'] ?>
                        </td>
                        <td>  
                            <a href="<?php echo   '/Special:QRClientsPagePayment?'
                                                . http_build_query(array('action' => 'edit',
                                                                         'pagepayment_id' => $row['pagepayment_id'])); ?>">
                                Редактировать
                            </a>
                        </td>
                        <td>  
                            <a href="#" onclick="MAKEPOST('<?php echo   '/Special:QRClientsPagePayment?'
                                                                      . http_build_query(array('action' => 'delete',
                                                                                               'pagepayment_id' => $row['pagepayment_id'])); ?>')">
                                Удалить
                            </a>
                        </td>
                    </tr>
                <?php endwhile ?>
            </table>
            <a href="<?php echo   '/Special:QRClientsPagePayment?'
                                . http_build_query(array('action' => 'new',
                                                         'page_id' => $pageid,
                                                         'service_id' => $serviceid)); ?>">
               Добавить
            </a>
            
            <?php
            $cont = ob_get_contents();
            ob_end_clean();
            $wgOut->addHTML($cont);
            
        
        }

        function renderNothing($text = "Не могу отобразить содержимое") {
            global $wgOut;
            
            $wgOut->addWikiText("== $text ==");
        }

}
