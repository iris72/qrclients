<?php


class QRClientsCommon extends SpecialPage {
    function execute ( $par ) {
        global $wgUser, $wgOut;

        $this -> setHeaders();
        $username = $wgUser->getName();

        if ( $username == "Wikiadmin" ) {
            $this->rawExecute( $par );
        } else {
            $wgOut->addWikiText( " == You are not permited ==" );
        }
    }

    function renderNothing($text = "Не могу отобразить содержимое") {
        global $wgOut;
            
        $wgOut->addWikiText("== $text ==");
    }
}
