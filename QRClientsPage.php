<?php


class QRClientsPage extends QRClientsCommon {
        function __construct() {
                parent::__construct( 'QRClientsPage' );
//                wfLoadExtensionMessages('QRClientsPage');
        }
 
        function rawExecute( $par ) {
            global $wgRequest, $wgOut;
 
            $this->setHeaders();
            
            $pageid = $wgRequest->getText('page_id');
            $pgn = $wgRequest->getText('page_name');
            if ( empty($pageid) && empty($pgn) ) {
                $this->renderNothing( "Нет параметра page_id или page_name" );
                return;
            }
            if ( ! empty($pageid) ) {
                $t = Title::newFromID($pageid);
            } else {
                $t = Title::newFromText($pgn);
            }
            if ( ! $t->exists() ) {
                $this->renderNothing( "Нет страницы с id=$pageid" );
                return;
            }
            $pagename = $t->getText();
            $pageid = $t->getArticleID();
            $this->renderPayedList($pageid, $pagename);
        }

        function renderPayedList($pageid, $pagename) {
            global $wgOut;
            
            $dbr = wfGetDB(DB_SLAVE);
            $services = $dbr->select('qrclient_service', 
                                    array('service_id', 
                                          'service_name',
                                          'service_description'));
            $wgOut->addWikiText("== [[$pagename]] ==");
            
            $res2 = $dbr->select('qrclient_org_page',
                                 array('orgpage_page_id',
                                       'orgpage_org_id'),
                                 array('orgpage_page_id' => $pageid));
            $row2 = $res2->fetchRow();
            
            if ( empty($row2) ) {
                $wgOut->addWikiText("Страница не привязана ни к одной организации");
            } else {
                $wgOut->addHTML(  '<a href="/Special:QRClientsOrganisation?' 
                                . http_build_query(array('id' => $row2['orgpage_org_id']))
                                . '">Перейти к организации</a>');
            }
                                           
            $wgOut->addWikiText('=== Оплаченные услуги ===');
            ob_start();
            ?>
            <script type="text/javascript">
                function MAKEPOST(addr) {
                    yes = confirm('Удалить ?');
                    if (yes) {
                        $.post(addr, function() {
                            window.location.reload()
                        });
                    }
                }
            </script> 

            <table>
                <?php while($row = $services->fetchRow()): ?>
                    <tr>
                        <td>
                            <a href="<?php echo '/Special:QRClientsService?'
                                                   . http_build_query(array('service_id' => $row['service_id'])); ?>">
                               <?php echo $row['service_name']
                                         . " - "
                                         . $row['service_description']; ?> 
                            </a>
                        </td>
                        <td>
                            <a href="<?php echo  '/Special:QRClientsPagePayments?'
                                               . http_build_query(array('page_id' => $pageid,
                                                                        'service_id' => $row['service_id'])); ?>">
                              Список оплат
                            </a>  
                        </td>
                    </tr>
                <?php endwhile ?>
            </table>
            <?php
            $cont = ob_get_contents();
            ob_end_clean();
            $wgOut->addHTML($cont);
            
        
        }

        function renderNothing($text = "Не могу отобразить содержимое") {
            global $wgOut;
            
            $wgOut->addWikiText("== $text ==");
        }

}
