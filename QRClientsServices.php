<?php


class QRClientsServices extends QRClientsCommon {
        function __construct() {
                parent::__construct( 'QRClientsServices' );
//                wfLoadExtensionMessages('QRClientsServices');
        }
 
        function rawExecute( $par ) {
                global $wgRequest, $wgOut;
 
                $this->setHeaders();
 
                $dbr = wfGetDB( DB_SLAVE );
                $res = $dbr->select('qrclient_service',
                                    array('service_id',
                                          'service_name',
                                          'service_description'));
                                   
                ob_start();
                ?>
                <script type="text/javascript">
                    function MAKEPOST(addr) {
                        yes = confirm('Удалить организацию ?');
                        if (yes) {
                            $.post(addr, function() {
                                window.location.reload()
                            });
                        }
                    }
                </script> 

                <table>
                <?php while($row = $res->fetchRow()): ?>
                   <tr>
                       <td>
                           <a href="<?php echo "/Special:QRClientsService?" . http_build_query(
                                                                                     array( "service_id" => $row['service_id'] )) ?>">
                           <?php echo $row['service_name'] ?>
                           </a>
                       </td>
                       <td>
                           <a href="#" onclick="MAKEPOST('<?php echo 
                               "/Special:QRClientsService?"
                             . http_build_query(array('action' => 'delete',
                                                      'service_id' => $row['service_id']));?>')">
                            Удалить
                           </a>
                       </td>
                   <tr>
                <?php endwhile ?>
                </table>
                <a href="/Special:QRClientsService?<?php echo http_build_query(array('action' => 'new')) ?>">
                    Создать
                </a>
                <?php
                $cont = ob_get_contents();
                ob_end_clean();
                $wgOut->addHTML($cont);
        }
}
