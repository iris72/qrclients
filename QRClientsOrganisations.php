<?php


class QRClientsOrganisations extends QRClientsCommon {
        function __construct() {
                parent::__construct( 'QRClientsOrganisations' );
//                wfLoadExtensionMessages('QRClientsOrganisations');
        }
 
        function rawExecute( $par ) {
                global $wgRequest, $wgOut;
 
                $this->setHeaders();
 
                $dbr = wfGetDB( DB_SLAVE );
                $res = $dbr->select('qrclient_organisations', array('organisation_id', 'organisation_name'),
                                    '', 'DatabaseBase::select', 'order by organisation_name');
                ob_start();
                ?>
                <script type="text/javascript">
                    function MAKEPOST(addr) {
                        yes = confirm('Удалить организацию ?');
                        if (yes) {
                            $.post(addr, function() {
                                window.location.reload()
                            });
                        }
                    }
                </script> 

                <table>
                <?php while($row = $res->fetchRow()): ?>
                   <tr>
                       <td>
                           <a href="<?php echo "/Special:QRClientsOrganisation?" . http_build_query(
                                                                                     array( "id" => $row['organisation_id'] )) ?>">
                           <?php echo $row['organisation_name'] ?>
                           </a>
                       </td>
                       <td>
                           <a href="#" onclick="MAKEPOST('<?php echo 
                               "/Special:QRClientsOrganisation?"
                             . http_build_query(array('action' => 'delete',
                                                      'id' => $row['organisation_id']));?>')">
                            Удалить
                           </a>
                       </td>
                   <tr>
                <?php endwhile ?>
                </table>
                <a href="/Special:QRClientsOrganisation?<?php echo http_build_query(array('action' => 'new')) ?>">
                    Создать
                </a>
                <?php
                $cont = ob_get_contents();
                ob_end_clean();
                $wgOut->addHTML($cont);
        }
}
