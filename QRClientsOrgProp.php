<?php


class QRClientsOrgProp extends QRClientsCommon {
        function __construct() {
                parent::__construct( 'QRClientsOrgProp' );
//                wfLoadExtensionMessages('QRClientsOrgProp');
        }
 
        function rawExecute( $par ) {
                global $wgRequest;
 
                $this->setHeaders();
                $meth = $wgRequest->getMethod();
                switch ($meth) {
                    case "GET":
                        $this->methodGET();
                        break;
                    case "POST":
                        $this->methodPOST();
                        break;
                    default:
                        $this->renderNothing();
                }
                
        }
        
        function methodPOST() {
            global $wgRequest, $wgOut;
            
            $action = $wgRequest->getText('action');
            switch ($action) {
                case "create":
                    $this->actionCreate();
                    break;
                case "update":
                    $this->actionUpdate();
                    break;
                case "delete":
                    $this->actionDelete();
                    break;
                default:
                    $this->renderNothing("параметр action имеет не верное значение");
            }
        }
        
        function actionDelete() {
            global $wgRequest;
            $prop_id = $wgRequest->getText('property_id');
            if ( empty($prop_id) ){
                $this->renderNothing( 'не параметры указаны, нужно property_id' );
                return;
            }
            
            $dbr = wfGetDB(DB_MASTER);
            $res = $dbr->select('qrclient_org_propery', array('property_id', 'property_org_id'), array('property_id' => $prop_id));
            $row = $res->fetchRow();
            if ( empty($row) ) {
                $this->renderNothing( "нет свойства property_id=$prop_id" );
                return;
            }
            $dbr->delete('qrclient_org_propery', 
                         array('property_id' => $prop_id));
            $dbr->commit();
            header('Location: /Special:QRClientsOrganisation?' . http_build_query(array('id' => $row['property_org_id'])));
            exit;
        }
            
            
        
        function actionUpdate() {
            global $wgRequest;
            $prop_id = $wgRequest->getText('property_id');
            $propname = $wgRequest->getText( 'property_name' );
            $propval = $wgRequest->getText( 'property_value' );
            if ( empty($prop_id) || empty($propname) || empty($propval) ){
                $this->renderNothing( 'не параметры указаны, нужно property_id property_name property_value' );
                return;
            }
            
            $dbr = wfGetDB(DB_MASTER);
            $res = $dbr->select('qrclient_org_propery', array('property_id', 'property_org_id'), array('property_id' => $prop_id));
            $row = $res->fetchRow();
            if ( empty($row) ) {
                $this->renderNothing( "нет свойства property_id=$prop_id" );
                return;
            }
            $dbr->update('qrclient_org_propery', 
                         array('property_name' => $propname,
                               'property_value' => $propval), 
                         array('property_id' => $prop_id));
            $dbr->commit();
            header('Location: /Special:QRClientsOrganisation?' . http_build_query(array('id' => $row['property_org_id'])));
            exit;
        }
                        
        function actionCreate() {
            global $wgOut, $wgRequest;
            
            $orgid = $wgRequest->getText('organisation_id');
            $propname = $wgRequest->getText( 'property_name' );
            $propval = $wgRequest->getText( 'property_value' );
            if ( empty($orgid) || empty($propname) || empty($propval) ) {
                $this->renderNothing( 'не параметры указаны, нужно organisation_id property_name property_value' );
                return;
            }
            
            $dbr = wfGetDB(DB_MASTER);
            $dbr->insert('qrclient_org_propery', array('property_org_id' => $orgid,
                                                       'property_name' => $propname,
                                                       'property_value' => $propval));
            $id = $dbr->insertId();
            $dbr->commit();
            header('Location: /Special:QRClientsOrganisation?' . http_build_query(array('id' => $orgid)));
            exit;
        }
        
        function methodGET() {
            global $wgRequest, $wgOut;
            
            $action = $wgRequest->getText('action');
            switch ($action) {
                case "new":
                    $this->actionNew();
                    break;
                case "edit":
                    $this->actionEdit();
                    break;
                default:
                    $this->renderNothing("параметр action имеет не верное значение");
            }
        }
        
        function actionEdit() {
            global $wgRequest;
            
            $prop_id = $wgRequest->getText('property_id');
            if ( empty($prop_id) ) {
                $this->renderNothing( "нет параметра property_id" );
                return;
            }
            
            $dbr = wfGetDB(DB_SLAVE);
            $res = $dbr->select('qrclient_org_propery', array('property_org_id',
                                                              'property_name',
                                                              'property_value'),
                                                        array('property_id' => $prop_id));
            $row = $res->fetchRow();
            if ( empty($row) ) {
                $this->renderNothing( "нет свойства с id=$prop_id" );
                return;
            }
            
            $this->renderEditForm($prop_id, $row['property_org_id'], $row['property_name'], $row['property_value'], 'update', "Сохранить");
        }
        
        function actionNew() {
            global $wgRequest;
            $id = $wgRequest->getText('organisation_id');
            if ( empty($id) ) {
                $this->renderNothing( "Не указан organisation_id" );
                return;
            }
            
            $this->renderEditForm( "", $id, "", "", "create", "Создать" );
        }
        
        function renderEditForm( $id, $orgid, $name, $value, $action, $verb ) {
            global $wgOut;
            ob_start();
            ?>
            <form method="POST" action="/Special:QRClientsOrgProp?<?php echo http_build_query(array('action' => $action))?>">
                <input type="hidden" name="property_id" value="<?php echo $id ?>"></input>
                <input type="hidden" name="organisation_id" value="<?php echo $orgid ?>"></input>
                <input type="text" name="property_name" placeholder="Название свойства" value="<?php echo $name ?>"></input>
                <input type="text" name="property_value" placeholder="Значение" value="<?php echo $value ?>"></input>
                <input type="submit" value="<?php echo $verb ?>"></input>
            </form>
            <?php
            $cont = ob_get_contents();
            ob_end_clean();
            $wgOut->addHTML($cont);
        }      
            
            
        
        function renderProperties( $id ) {
            global $wgOut;
            $dbr = wfGetDB(DB_SLAVE);
            $res = $dbr->select('qrclient_org_propery', array('property_id', 'property_name', 'property_value'),
                                array('property_org_id' => $id));
            
            
            ob_start();
            ?>
            <ul>
                <?php while($row = $res->fetchRow()): ?>
                <li>
                    <a href="<?php echo '/Special:QRClientsOrgProp?' . http_build_query(
                                                                       array('id' => $row['property_id']))?>">
                    <?php echo $row['property_name'] . ': ' . $row['property_value'] ?>
                    </a>
                    
                </li>    
                <?php endwhile ?>
            </ul>
            <?php
            $cont = ob_get_contents();
            ob_end_clean();
            $wgOut->addHTML($cont);
        }
        
        function renderPages( $id ) {
            global $wgOut;
            $dbr = wfGetDB(DB_SLAVE);
            $res = $dbr->select('qrclient_org_page', array('orgpage_id', 'orgpage_page_id'),
                                array('orgpage_org_id' => $id));
            
            
            ob_start();
            ?>
            <ul>
                <?php while($row = $res->fetchRow()): ?>
                <li>
                    <a href="<?php echo '/Special:QRClientsOrgPage?' . http_build_query(
                                                                       array('id' => $row['orgpage_id']))?>">
                    <?php echo $this->getPageName($row['orgpage_page_id']) ?>
                    </a>
                    
                </li>    
                <?php endwhile ?>
            </ul>
            <?php
            $cont = ob_get_contents();
            ob_end_clean();
            $wgOut->addHTML($cont);
            
        }
        
        function getPageName( $pid ) {
            $a = Article::newFromID( $pid );
            $t = $a->getTitle();
            return $t->getText();
        }
        
        function renderNothing($text = "Не могу отобразить содержимое") {
            global $wgOut;
            
            $wgOut->addWikiText("== $text ==");
        }
}
