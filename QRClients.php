<?php

 
   $wgExtensionCredits['specialpage'][] = array(
       'path' => __FILE__,
       'name' => 'QRClients',
       'author' =>'Iris Integration', 
       'description' => 'Extension to manage organizations and their parameters',
       'version'  => 1.0,
   );
   
    # Schema updates for update.php
    $wgHooks['LoadExtensionSchemaUpdates'][] = 'QRClientsUpdate';
    function QRClientsUpdate( DatabaseUpdater $updater ) {
            $updater->addExtensionTable( array('addTable', 'qrclient_organisations',
                    dirname( __FILE__ ) . 'schema/table.sql', false ));
            return true;
    };
    
    $dir = dirname(__FILE__) . '/';
    
    $wgAutoloadClasses['QRClientsCommon'] = $dir . 'QRClientsCommon.php';
    $wgAutoloadClasses['QRClientsOrganisations'] = $dir . 'QRClientsOrganisations.php';
    $wgAutoloadClasses['QRClientsOrganisation'] = $dir . 'QRClientsOrganisation.php';
    $wgAutoloadClasses['QRClientsOrgProp'] = $dir . 'QRClientsOrgProp.php';
    $wgAutoloadClasses['QRClientsOrgPage'] = $dir . 'QRClientsOrgPage.php';
    $wgAutoloadClasses['QRClientsPagePayment'] = $dir . 'QRClientsPagePayment.php';
    $wgAutoloadClasses['QRClientsPagePayments'] = $dir . 'QRClientsPagePayments.php';
    $wgAutoloadClasses['QRClientsService'] = $dir . 'QRClientsService.php';
    $wgAutoloadClasses['QRClientsServices'] = $dir . 'QRClientsServices.php';
    $wgAutoloadClasses['QRClientsPage'] = $dir . 'QRClientsPage.php';
    $wgAutoloadClasses['QRClientsMain'] = $dir . 'QRClientsMain.php';

    
    $wgSpecialPages['QRClientsOrganisations'] = 'QRClientsOrganisations';
    $wgSpecialPages['QRClientsOrganisation'] = 'QRClientsOrganisation';
    $wgSpecialPages['QRClientsOrgProp'] = 'QRClientsOrgProp';
    $wgSpecialPages['QRClientsOrgPage'] = 'QRClientsOrgPage';
    $wgSpecialPages['QRClientsPagePayment'] = 'QRClientsPagePayment';
    $wgSpecialPages['QRClientsPagePayments'] = 'QRClientsPagePayments';
    $wgSpecialPages['QRClientsService'] = 'QRClientsService';       
    $wgSpecialPages['QRClientsServices'] = 'QRClientsServices';                                                         
    $wgSpecialPages['QRClientsPage'] = 'QRClientsPage';
    $wgSpecialPages['QRClientsMain'] = 'QRClientsMain';
    