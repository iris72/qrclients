<?php


class QRClientsOrgPage extends QRClientsCommon {
        function __construct() {
                parent::__construct( 'QRClientsOrgPage' );
//                wfLoadExtensionMessages('QRClientsOrgPage');
        }
 
        function rawExecute( $par ) {
                global $wgRequest;
 
                $this->setHeaders();
                $meth = $wgRequest->getMethod();
                switch ($meth) {
                    case "GET":
                        $this->methodGET();
                        break;
                    case "POST":
                        $this->methodPOST();
                        break;
                    default:
                        $this->renderNothing();
                }
                
        }
        
        function methodPOST() {
            global $wgRequest, $wgOut;
            
            $action = $wgRequest->getText('action');
            switch ($action) {
                case "create":
                    $this->actionCreate();
                    break;
                case "update":
                    $this->actionUpdate();
                    break;
                case "delete":
                    $this->actionDelete();
                    break;
                default:
                    $this->renderNothing("параметр action имеет не верное значение");
            }
        }
        
        function actionDelete() {
            global $wgRequest;
            $orgpage = $wgRequest->getText('orgpage_id');
            if ( empty($orgpage) ){
                $this->renderNothing( 'не параметры указаны, нужно orgpage_id' );
                return;
            }
            
            $dbr = wfGetDB(DB_MASTER);
            $res = $dbr->select('qrclient_org_page', array('orgpage_page_id', 'orgpage_org_id')
                                                   , array('orgpage_id' => $orgpage));
            $row = $res->fetchRow();
            if ( empty($row) ) {
                $this->renderNothing( "нет свойства property_id=$prop_id" );
                return;
            }
            $dbr->delete('qrclient_org_page', 
                         array('orgpage_id' => $orgpage));
            $dbr->commit();
            header('Location: /Special:QRClientsOrganisation?' . http_build_query(array('id' => $row['orgpage_org_id'])));
            exit;
        }
            
            
        
        function actionUpdate() {
            global $wgRequest;
            
            $orgpage = $wgRequest->getText('orgpage_id');
            $pagename = $wgRequest->getText( 'orgpage_name' );

            if ( empty($orgpage) || empty($pagename) ){
                $this->renderNothing( 'нет параметра orgpage_id, orgpage_name' );
                return;
            }
            
            $dbr = wfGetDB(DB_MASTER);
            $res = $dbr->select('qrclient_org_page', array('orgpage_page_id', 'orgpage_org_id'), 
                                                     array('orgpage_id' => $orgpage));
            $row = $res->fetchRow();
            if ( empty($row) ) {
                $this->renderNothing( "нет свойства property_id=$prop_id" );
                return;
            }
            
            $t = Title::newFromText($pagename);
            if (! $t->exists()) {
                $this->renderNothing( "Такая страница еще не создана" );
                return;
            }
            $pageid = $t->getArticleID();
            
            
            $dbr->update('qrclient_org_page', 
                         array('orgpage_page_id' => $pageid),
                         array('orgpage_id' => $orgpage));
            
            
            $dbr->commit();
            header('Location: /Special:QRClientsOrganisation?' . http_build_query(array('id' => $row['orgpage_org_id'])));
            exit;
        }
                        
        function actionCreate() {
            global $wgOut, $wgRequest;
            
            $orgid = $wgRequest->getText('organisation_id');
            $pagename = $wgRequest->getText( 'orgpage_name' );
            if ( empty($orgid) || empty($pagename) ) {
                $this->renderNothing( 'не параметры указаны, нужно organisation_id orgpage_name' );
                return;
            }
            
            $t = Title::newFromText($pagename);
            if (! $t->exists()) {
                $this->renderNothing( "Такая страница еще не создана" );
                return;
            }
            $pageid = $t->getArticleID();
            
            $dbr = wfGetDB(DB_MASTER);
            $dbr->insert('qrclient_org_page', array('orgpage_org_id' => $orgid,
                                                    'orgpage_page_id' => $pageid));
            $id = $dbr->insertId();
            $dbr->commit();
            header('Location: /Special:QRClientsOrganisation?' . http_build_query(array('id' => $orgid)));
            exit;
        }
        
        function methodGET() {
            global $wgRequest, $wgOut;
            
            $action = $wgRequest->getText('action');
            switch ($action) {
                case "new":
                    $this->actionNew();
                    break;
                case "edit":
                    $this->actionEdit();
                    break;
                case "show":
                case "":
                    $this->actionShow();
                    break;
                default:
                    $this->renderNothing("параметр action имеет не верное значение");
            }
        }
        
        function actionShow() {
            global $wgRequest, $wgOut;
            
            $orgpage = $wgRequest->getText('orgpage_id');
            if ( empty($orgpage) ){
                $this->renderNothing( "нет парметра orgpage_id" );
                return;
            }
            
            $dbr = wfGetDB(DB_SLAVE);
            $res = $dbr->select('qrclient_org_page',
                                array('orgpage_page_id', 'orgpage_org_id'),
                                array('orgpage_id' => $orgpage));
            $row = $res->fetchRow();
            if ( empty($row) ){
                $this->renderNothin( "нет привязки id=$orgpage" );
                return;
            }
            $pageid = $row['orgpage_page_id'];
            $t = Title::newFromID($pageid);
            if ( ! $t->exists() ) {
                $this->renderNothing( "нет такой страницы" );
                return;
            }
            
            $pagename = $t->getText();
            
            $wgOut->addWikiText("== [[$pagename]] ==");
            $wgOut->addHTML(  '<p><a href="/Special:QRClientsOrganisation?'
                            . http_build_query(array('id' => $row['orgpage_org_id']))
                            . '">Назад к организации</a></p>');
            $wgOut->addHTML(  '<p><a href="/Special:QRClientsPage?'
                            . http_build_query(array('page_id' => $pageid))
                            . '">Перейти к списку услуг</a></p>'); 
            $wgOut->addHTML(  '<a href="/Special:QRClientsOrgPage?' 
                            . http_build_query(array('action' => 'edit',
                                                     'orgpage_id' => $orgpage)) 
                            . '">Сменить страницу</a>');
        }
        
        function actionEdit() {
            global $wgRequest;
            
            $orgpage = $wgRequest->getText('orgpage_id');
            
            if ( empty($orgpage) ){
                $this->renderNothing( "нет параметра orgpage_id" );
                return;
            }
            
            $dbr = wfGetDB(DB_SLAVE);
            $res = $dbr->select('qrclient_org_page', array('orgpage_page_id',
                                                           'orgpage_org_id')
                                                   , array('orgpage_id' => $orgpage));
            $row = $res->fetchRow();
            if ( empty($row) ) {
                $this->renderNothing( "нет привязки с id=$orgpage" );
                return;
            }
            $pagename = $this->getPageName($row['orgpage_page_id']);
            
            $this->renderEditForm($orgpage, $row['orgpage_org_id'], $pagename, 'update', 'Изменить');
        }
        
        function actionNew() {
            global $wgRequest;
            $id = $wgRequest->getText('organisation_id');
            if ( empty($id) ) {
                $this->renderNothing( "Не указан organisation_id" );
                return;
            }
            
            $this->renderEditForm( "", $id, "", "create", "Привязать" );
        }
        
        function renderEditForm( $id, $orgid, $name, $action, $verb ) {
            global $wgOut;
            ob_start();
            ?>
            <form method="POST" action="/Special:QRClientsOrgPage?<?php echo http_build_query(array('action' => $action))?>">
                <input type="hidden" name="orgpage_id" value="<?php echo $id ?>"></input>
                <input type="hidden" name="organisation_id" value="<?php echo $orgid ?>"></input>
                <input type="text" name="orgpage_name" placeholder="Название статьи" value="<?php echo $name ?>"></input>
                <input type="submit" value="<?php echo $verb ?>"></input>
            </form>
            <?php
            $cont = ob_get_contents();
            ob_end_clean();
            $wgOut->addHTML($cont);
        }      
            
        
        function getPageName( $pid ) {
            $a = Article::newFromID( $pid );
            $t = $a->getTitle();
            return $t->getText();
        }
        
        function renderNothing($text = "Не могу отобразить содержимое") {
            global $wgOut;
            
            $wgOut->addWikiText("== $text ==");
        }
}
