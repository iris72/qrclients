CREATE TABLE qrclient_organisations (
      organisation_id int(10) unsigned not null PRIMARY KEY AUTO_INCREMENT,
      organisation_name varchar(200) not null,
      organisation_create_date timestamp not null default now()
) /*$wgDBTableOptions*/;
CREATE TABLE qrclient_org_propery (
       property_id int(10) unsigned not null PRIMARY KEY AUTO_INCREMENT,
       property_org_id int(10) unsigned not null,
       property_name varchar(200) not null,
       property_value varchar(200) not null,
       property_create_date timestamp not null default now()
)  /*$wgDBTableOptions*/;
CREATE TABLE qrclient_org_page (
       orgpage_id int(10) unsigned not null PRIMARY KEY AUTO_INCREMENT,
       orgpage_page_id int(10) unsigned not null,
       orgpage_org_id int(10) unsigned not null,
       orgpage_create_date timestamp not null default now(),
       UNIQUE (orgpage_page_id)
)  /*$wgDBTableOptions*/;
CREATE TABLE qrclient_page_payment (
       pagepayment_id int(10) unsigned not null PRIMARY KEY AUTO_INCREMENT,
       pagepayment_page_id int(10) unsigned not null,
       pagepayment_service_id int(10) unsigned not null,
       pagepayment_start_date date not null,
       pagepayment_termination_date date not null,
       pagepayment_create_date timestamp not null default now(),
       UNIQUE (pagepayment_page_id, pagepayment_service_id, pagepayment_start_date, pagepayment_termination_date)
)  /*$wgDBTableOptions*/;

CREATE TABLE qrclient_service (
       service_id int(10) unsigned not null PRIMARY KEY AUTO_INCREMENT,
       service_name varchar(200) not null,
       service_description text,
       create_date timestamp not null default now(),
       UNIQUE (service_name)
)  /*$wgDBTableOptions*/;

ALTER TABLE qrclient_org_propery  ADD CONSTRAINT FOREIGN KEY (property_org_id)        REFERENCES qrclient_organisations(organisation_id) ON DELETE CASCADE;
ALTER TABLE qrclient_org_page     ADD CONSTRAINT FOREIGN KEY (orgpage_page_id)        REFERENCES page(page_id)                           ON DELETE CASCADE;
ALTER TABLE qrclient_org_page     ADD CONSTRAINT FOREIGN KEY (orgpage_org_id)         REFERENCES qrclient_organisations(organisation_id) ON DELETE CASCADE;
ALTER TABLE qrclient_page_payment ADD CONSTRAINT FOREIGN KEY (pagepayment_page_id)    REFERENCES page(page_id)                           ON DELETE CASCADE;
ALTER TABLE qrclient_page_payment ADD CONSTRAINT FOREIGN KEY (pagepayment_service_id) REFERENCES qrclient_service(service_id)            ON DELETE CASCADE;


CREATE INDEX qrclient_organisation_name ON qrclient_organisations(organisation_name);
CREATE INDEX qrclient_org_propery_name ON qrclient_org_propery(property_name);
CREATE INDEX qrclient_org_propery_value ON qrclient_org_propery(property_value);
CREATE INDEX qrclient_page_payment_termination_date ON qrclient_page_payment(pagepayment_termination_date);
CREATE INDEX qrclient_page_payment_start_date ON qrclient_page_payment(pagepayment_start_date);
