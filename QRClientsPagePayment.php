<?php


class QRClientsPagePayment extends QRClientsCommon {
        function __construct() {
                parent::__construct( 'QRClientsPagePayment' );
//                wfLoadExtensionMessages('QRClientsPagePayment');
        }
 
        function rawExecute( $par ) {
                global $wgRequest;
 
                $this->setHeaders();
                $meth = $wgRequest->getMethod();
                switch ($meth) {
                    case "GET":
                        $this->methodGET();
                        break;
                    case "POST":
                        $this->methodPOST();
                        break;
                    default:
                        $this->renderNothing();
                }
                
        }
        
        function methodPOST() {
            global $wgRequest, $wgOut;
            
            $action = $wgRequest->getText('action');
            switch ($action) {
                case "create":
                    $this->actionCreate();
                    break;
                case "update":
                    $this->actionUpdate();
                    break;
                case "delete":
                    $this->actionDelete();
                    break;
                default:
                    $this->renderNothing("параметр action имеет не верное значение");
            }
        }
        
        function actionDelete() {
            global $wgRequest;
            $payid = $wgRequest->getText('pagepayment_id');
            if ( empty($payid) ) {
                $this->renderNothing( 'не параметры указаны: pagepayment_id' );
                return;
            }
            
            $dbr = wfGetDB(DB_MASTER);
            $res = $dbr->select('qrclient_page_payment',
                                array('pagepayment_page_id', 'pagepayment_service_id'),
                                array('pagepayment_id' => $payid));
            $row = $res->fetchRow();
            if ( empty($row) ) {
                $this->renderNothing( "нет оплаты pagepayment_id=$payid" );
                return;
            }
            $dbr->delete('qrclient_page_payment',
                         array('pagepayment_id' => $payid));
            $dbr->commit();
            
            $this->goUP($row['pagepayment_page_id'], $row['pagepayment_service_id']);
        }
            
            
        
        function actionUpdate() {
            global $wgOut, $wgRequest;

            
            $start = $wgRequest->getText('pagepayment_start_date');
            $term = $wgRequest->getText('pagepayment_termination_date');
            $payid = $wgRequest->getText('pagepayment_id');
            
            if ( empty($start) || empty($term) || empty($payid) ) {
                $this->renderNothing( 'не все параметры указаны, нужно еще pagepayment_start_date, pagepayment_termination_date, pagepayment_id' );
                return;
            }
            
            $dbr = wfGetDB(DB_MASTER);            
            
            $res = $dbr->select('qrclient_page_payment',
                                array('pagepayment_id',
                                      'pagepayment_page_id',
                                      'pagepayment_service_id'),
                                array('pagepayment_id' => $payid));
            $row = $res->fetchRow();
            if ( empty($row) ) {
                $this->renderNothing( "нет оплаты с id=$payid" );
                return;
            }
            $dbr->update('qrclient_page_payment',
                         array('pagepayment_start_date' => $start,
                               'pagepayment_termination_date' => $term),
                         array('pagepayment_id' => $payid));
            $dbr->commit();
            $this->goUP( $row['pagepayment_page_id'], $row['pagepayment_service_id'] );
        }
                        
        function actionCreate() {
            global $wgOut, $wgRequest;

            $r = $this->getPageAndService();
            if ( $r === FALSE ) {
                return;
            }
            
            list($pageid, $pagename, $serviceid, $servicename) = $r;
            
            $start = $wgRequest->getText('pagepayment_start_date');
            $term = $wgRequest->getText('pagepayment_termination_date');
            
            
            if ( empty($start) || empty($term) ) {
                $this->renderNothing( 'не все параметры указаны, нужно еще pagepayment_start_date, pagepayment_termination_date' );
                return;
            }
            
            $dbr = wfGetDB(DB_MASTER);
            
            $dbr->insert('qrclient_page_payment',
                         array('pagepayment_page_id' => $pageid,
                               'pagepayment_service_id' => $serviceid,
                               'pagepayment_start_date' => $start,
                               'pagepayment_termination_date' => $term));
            $id = $dbr->insertId();
            $dbr->commit();
            $this->goUP($pageid, $serviceid);
        }

        function goUP($pageid, $serviceid) {
            $loc =   '/Special:QRClientsPagePayments?'
                       . http_build_query(array('page_id' => $pageid,
                                                'service_id' => $serviceid));
            header('Location: ' . $loc);
            exit;
        }
        
        function methodGET() {
            global $wgRequest, $wgOut;
            
            $action = $wgRequest->getText('action');
            switch ($action) {
                case "new":
                    $this->actionNew();
                    break;
                case "edit":
                    $this->actionEdit();
                    break;
                default:
                    $this->renderNothing("параметр action имеет не верное значение");
            }
        }
        
        function actionEdit() {
            global $wgRequest;
            
            $payid = $wgRequest->getText('pagepayment_id');
            if ( empty($payid) ) {
                $this->renderNothing( "нет параметра pagepayment_id" );
                return;
            }
            
            $dbr = wfGetDB(DB_SLAVE);
            $res = $dbr->select('qrclient_page_payment', array('pagepayment_page_id', 
                                                               'pagepayment_service_id',
                                                               'pagepayment_start_date',
                                                               'pagepayment_termination_date'),
                                array('pagepayment_id' => $payid));
            $row = $res->fetchRow();
            if ( empty($row) ) {
                $this->renderNothing( "нет оплаты с id=$payid" );
                return;
            }
            
            $this->renderEditForm( $payid, "", "", 
                                   $row['pagepayment_start_date'],
                                   $row['pagepayment_termination_date'],
                                   "update",
                                   "Обновить" );
            
        }

        function getPageAndService() {
            global $wgRequest;
            
            $pageid = $wgRequest->getText('page_id');
            $serviceid = $wgRequest->getText('service_id');
            
            if ( empty($pageid) || empty($serviceid) ) {
                $this->renderNothing( "не указан параметр page_id или service_id" );
                return FALSE;
            }
            
            $t = Title::newFromID($pageid);
            if ( ! $t->exists() ) {
                $this->renderNothing( "страница не существует id=$pageid" );
                return FALSE;
            }
            $pagename = $t->getText();
            
            $dbr = wfGetDB(DB_SLAVE);
            $res = $dbr->select('qrclient_service',
                                array('service_id'),
                                array('service_id' => $serviceid));
            $row=$res->fetchRow();
            if ( empty($row) ) {
                $this->renderNothing( "нет услуги с id=$serviceid" );
                return FALSE;
            }
            $servicename = $row['service_name'];
            return array($pageid, $pagename, $serviceid, $servicename);
        }   
                                      
            
            
            
        
        function actionNew() {
            global $wgRequest;
            
            $r = $this->getPageAndService();
            if ( $r === FALSE ) {
                return;
            }
            
            list( $pageid, $pagename, $serviceid, $servicename ) = $r;
            
            $this->renderEditForm( "", $pageid, $serviceid, "", "", "create", "Создать" );
        }
        
        function renderEditForm( $payid, $pageid, $serviceid, $start, $term, $action, $verb ) { 
            global $wgOut;
            ob_start();
            ?>
            <form method="POST" action="/Special:QRClientsPagePayment?<?php echo http_build_query(array('action' => $action))?>">
                <?php if ( ! empty($payid) ): ?>
                    <input type="hidden" name="pagepayment_id" value="<?php echo $payid ?>"></input>
                <?php endif ?>
                <input type="hidden" name="page_id" value="<?php echo $pageid ?>"></input>
                <input type="hidden" name="service_id" value="<?php echo $serviceid ?>"></input>
                
                
                <input type="text" name="pagepayment_start_date" placeholder="Начало срока оплаты" value="<?php echo $start ?>"></input>
                <input type="text" name="pagepayment_termination_date" placeholder="Окончание срока оплаты" value="<?php echo $term ?>"></input>
                <input type="submit" value="<?php echo $verb ?>"></input>
            </form>
            <?php
            $cont = ob_get_contents();
            ob_end_clean();
            $wgOut->addHTML($cont);
        }      

        
        function renderNothing($text = "Не могу отобразить содержимое") {
            global $wgOut;
            
            $wgOut->addWikiText("== $text ==");
        }
}
