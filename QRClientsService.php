<?php


class QRClientsService extends SpecialPage {
        function __construct() {
                parent::__construct( 'QRClientsService' );
//                wfLoadExtensionMessages('QRClientsService');
        }
 
        function execute( $par ) {
                global $wgRequest;
 
                $this->setHeaders();
                $meth = $wgRequest->getMethod();
                switch ($meth) {
                    case "GET":
                        $this->methodGET();
                        break;
                    case "POST":
                        $this->methodPOST();
                        break;
                    default:
                        $this->renderNothing();
                }
                
        }
        
        function methodPOST() {
            global $wgRequest, $wgOut;
            
            $action = $wgRequest->getText('action');
            switch ($action) {
                case "create":
                    $this->actionCreate();
                    break;
                case "update":
                    $this->actionUpdate();
                    break;
                case "delete":
                    $this->actionDelete();
                    break;
                default:
                    $this->renderNothing("параметр action имеет не верное значение");
            }
        }
        
        function actionUpdate() {
            global $wgRequest;
            $id = $wgRequest->getText('organisation_id');
            $name = $wgRequest->getText('organisation_name');
            $desc = $wgRequest->getText('service_description');
            
            if ( empty($id) || empty($name) || empty($desc) ) {
                $this->renderNothing( "не указан один из параметров organisation_id, organisation_name, service_description" );  
                return;
            }
            
            $dbr = wfGetDB(DB_MASTER);
            $res = $dbr->select('qrclient_service',
                                array('service_name', 'service_description'),
                                array('service_id' => $id));
            $row = $res->fetchRow();
            if ( empty($row) ) {
                $this->renderNothing( "нет услуги id=$id" );
                return;                
            }
            
            $dbr->update('qrclient_service',
                         array('service_name' => $name, 'service_description' => $desc),
                         array('service_id' => $id));
            $dbr->commit();
            
            header('Location: /Special:QRClientsService?' . http_build_query(array('service_id' => $id)));
            exit;
            
        }
        
        function actionDelete() {
            global $wgRequest;
            $id = $wgRequest->getText('service_id');
            if ( empty($id) ) {
                $this->renderNothing( "не указан параметр service_id" );
                return;
            }
            
            $dbr = wfGetDB(DB_MASTER);
            $res = $dbr->select('qrclient_service',
                                array('service_name'),
                                array('service_id' => $id));
            $row = $res->fetchRow();
            if ( empty($row) ) {
                $this->renderNothing( "нет услуги id=$id" );
                return;                
            }
            $dbr->delete('qrclient_service',
                         array('service_id' => $id));
            $dbr->commit();
            
            header('Location: /Special:QRClientsServices');
            exit;
            
        }
        
        function actionCreate() {
            global $wgOut, $wgRequest;
            
            $name = $wgRequest->getText('service_name');
            $desc = $wgRequest->getText('service_description');
            if ( empty($name) || empty($desc) ) {
                $this->renderNothing('не указано service_name или service_description');
                return;
            }
            
            $dbr = wfGetDB(DB_MASTER);
            $dbr->insert('qrclient_service', array('service_name' => $name,
                                                   'service_description' => $desc));
            $id = $dbr->insertId();
            $dbr->commit();
            header('Location: /Special:QRClientsService?' . http_build_query(array('service_id' => $id)));
            exit;
        }
        
        function methodGET() {
            global $wgRequest, $wgOut;
            
            $action = $wgRequest->getText('action');
            switch ($action) {
                case "new":
                    $this->actionNew();
                    break;
                case "edit":
                    $this->actionEdit();
                    break;
                case "show":
                case "":
                    $this->actionShow();
                    break;
                default:
                    $this->renderNothing("параметр action имеет не верное значение");
            }
        }
        
        function actionEdit() {
            global $wgRequest;
            $id = $wgRequest->getText('id');
            if ( empty($id) ) {
                $this->renderNothing( "не указан параметр id" );
                return;
            }
            
            $dbr = wfGetDB(DB_SLAVE);
            $res = $dbr->select('qrclient_service',
                                array('service_name',
                                      'service_description',
                                      'service_id'),
                                array('service_id' => $id));
            
            $row = $res->fetchRow();
            if ( empty($row) ) {
                $this->renderNothing( "нет услуги id=$id" );
                return;                
            }
            
            $sname = $row['service_name'];
            $sdesc = $row['service_description'];
            $this->renderEditForm( $id, $sname, $sdesc, "update", "Обновить" );
        }
        
        function actionNew() {
            $this->renderEditForm( "", "", "", "create", "Создать" );
        }
        
        function renderEditForm( $id, $name, $desc, $action, $verb ) {
            global $wgOut;
            
            ob_start();
            ?>
            <form method="POST" action="/Special:QRClientsService?action=<?php echo $action ?>">
                <?php if ( ! empty($id) ): ?>
                    <input type="hidden" name="service_id" value="<?php echo $id ?>"></input>
                <?php endif ?>    
                <input type="text" name="service_name" placeholder="Название услуги" value="<?php echo $name ?>"></input>
                <input type="text" name="service_description" placeholder="Описание услуги" rows="10" value="<?php echo $desc ?>"></input>
                <input type="submit" value="<?php echo $verb ?>"></input>
            </form>
            <?php
            $cont = ob_get_contents();
            ob_end_clean();
            $wgOut->addHTML($cont);
        }
        
        function actionShow() {
            global $wgRequest, $wgOut;
            $id = $wgRequest->getText('service_id');
            if (empty($id)) {
                $this->renderNothing( "нет параметра service_id" );
                return;
            }
            
            $dbr = wfGetDB( DB_SLAVE );
            $res = $dbr->select('qrclient_service', 
                                array('service_description',
                                      'service_name'),
                                array('service_id' => $id));
            $row = $res->fetchRow();
            if (empty($row)) {
                $this->renderNothing("нет организации с id = $id");
                return;
            }
            $name = $row['service_name'];
            $desc = $row['service_description'];
            
            $wgOut->addHTML( '<a href="/Special:QRClientsServices">К списку</a>' );
            $wgOut->addWikiText( "== $name ==" );
            $wgOut->addWikiText( $desc );
            $this->renderPages($id);
        }
        

        
        function renderPages( $id ) {
            global $wgOut;
            $dbr = wfGetDB(DB_SLAVE);
            $res = $dbr->select('qrclient_page_payment',
                                array('pagepayment_page_id',
                                      'max(pagepayment_termination_date)'),
                                array('pagepayment_service_id' => $id),
                                'DatabaseBase::select',
                                array('GROUP BY' => 'pagepayment_page_id',
                                      'ORDER BY' => 'pagepayment_termination_date'));
            
            error_log(print_r($dbr->lastQuery(), true));
            
            $wgOut->addWikiText( "=== Оплаченные страницы ===" );
            ob_start();
            ?>
            <table>
                <?php while($row = $res->fetchRow()): ?>
                </tr>
                <?php 
                $t = Title::newFromID($row['pagepayment_page_id']);
                if ( $t->exists() ):
                    $pagename = $t->getText(); ?>
                    <td>
                        <a href="<?php echo '/Special:QRClientsPagePayments?' . http_build_query(
                                                                                  array('page_id' => $row['pagepayment_page_id'],
                                                                                        'service_id' => $id)) ?>">
                        <?php echo $pagename ?>
                        </a>
                    </td>
                    <td>
                        <?php echo $row['pagepayment_termination_date'] ?>
                    </td>
                </tr>
                <?php endif ?>    
                <?php endwhile ?>
            </table>
            <?php
            $cont = ob_get_contents();
            ob_end_clean();
            $wgOut->addHTML($cont);
            
        }
        
        function renderNothing($text = "Не могу отобразить содержимое") {
            global $wgOut;
            
            $wgOut->addWikiText("== $text ==");
        }
}
